import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.Optional;
import java.util.Queue;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;


public class Main {

	public static void main(String[] args) {
		BufferedReader buff=new BufferedReader(new InputStreamReader(System.in));
		int numtries;
		try {
		numtries=Integer.valueOf(buff.readLine().trim());
		while(0<=--numtries) {
			buff.readLine();
			int dimens[]=readDimensions(buff);
			char []mat []=loadMatrix(dimens[0],dimens[1], buff);
			int numbwor=numberWord(buff);
			String words[]=readWord(buff, numbwor);
			//System.out.println("inicia el main");
			Main.Game game=new Main().new Game(mat,words);
			if(numtries!=0)System.out.println();
		 }
		}catch (Exception e) {
			//e.printStackTrace();
			System.exit(-1);
		}
	}
	
	
	
	static char [][] loadMatrix(int rows,int colmns,BufferedReader buff) throws IOException{
		char []matr[]=new char[rows][colmns];
		int read_char=0;
		IntUnaryOperator unirop=Character::toLowerCase;
		int controw=0, contcol=0;
		while(controw !=rows && (read_char=buff.read())!=-1) {
			if( (char)read_char =='\r') continue;
			if(((char)read_char)=='\n') {contcol=0;++controw;continue;}
			//System.out.println("-----------xxx------"+(char)read_char+"-------------");
			matr[controw][contcol++]=(char)unirop.applyAsInt(read_char);
		}
		return matr;
	}
	
	static int numberWord(BufferedReader byff) throws IOException {
		return Integer.parseInt(byff.readLine());
	}
	
	
	static String []readWord(BufferedReader buff,int numwords) throws IOException {
		String lin;
		String lines[]=new String[numwords];
		int cop=0;
			while( numwords !=0 &&  null!=(lin=buff.readLine()) ) {
				lines[cop]=lin.toLowerCase();
				++cop;
				--numwords;
			}
		return lines;
	}
	
	static int[] readDimensions(BufferedReader buff) throws IOException{
		String[] lin=buff.readLine().split(" ");
		int a=Integer.parseInt(lin[0]),b=Integer.parseInt(lin[1]);
		return new int[]{a,b};
	}
	
class Game{
		
		final private char [][]grid;
		final private String[] wordToFind;
		java.util.List<Nod> firstNode;
		Predicate<String> pred=s->s.length()%2!=0;
		Predicate<char []>pre=s->s.length%2!=0;
		Predicate<char [][]>prem=s->s.length%2!=0;
		BiPredicate<Integer,Integer> check=(x,y)->y>=0 && y <= x;
		java.util.Map<String,java.util.List<Nod>> mapList;
		Function<String,Optional<java.util.List<Nod>>> searKey;
		
		
		Game(char mat[][],String word []){
			grid=mat;
			wordToFind=word;
			firstNode=new ArrayList<Main.Nod>();
			findWords(0);
			mapList=new java.util.HashMap();
		}
		
		void findLetter(int idexArray,char let$) {
			// return a boolean if you find  in the first row
			//if the length of word is one, the process is diferent
			//boolean isodd=pred.test(word);
			//int mid=isodd?((wordToFind.length)/2):((wordToFind.length-1)/2);
			//if you find the word in up row dont find out down
			boolean isodd=prem.test(grid);
			int mid=isodd?((grid.length)/2):((grid.length-1)/2);
			for(int i=0,j=grid.length-1;i<=mid &&j>=mid;i++,j--) {
				if(i==j) {
					//System.out.println("Middle row--"+Arrays.toString(grid[i])+"--------");
					searchLetter(idexArray,i,grid[i], let$);continue;
				}	
				//System.out.println("first row--"+Arrays.toString(grid[i])+"--------");
				searchLetter(idexArray,i, grid[i], let$); 
				//System.out.println("last row"+Arrays.toString(grid[j])+"--------------");
				searchLetter(idexArray,j,grid[j], let$);	
			}
		}
		
		void findWords(int index) {
			if(index == wordToFind.length) return;
			//System.out.println("search words-----------"+wordToFind[index]+"----------");
			mapList=new java.util.HashMap();
			findLetter(index,wordToFind[index].charAt(0));
			Comparator<Nod>com=Comparator.comparing(Nod::getPosRow).thenComparing(Nod::getPosCol);
			mapList.get(wordToFind[index]).stream().sorted(com).findFirst().ifPresent(System.out::println);
			// order de list of the word and print its value ordered
			findWords(++index);
		}
		
		
		void searchLetter(int indexArrayWord,int indexRow,char [] row,char letter) {
			//if you find the word in the left dont search in the right 
			// if you find on left  return boolean
			boolean isodd=pre.test(row);
			int mid=isodd?((row.length)/2):((row.length-1)/2);
			for(int i=0,j=row.length-1;i<=mid &&j>=mid;i++,j--) {
				if(i==j) {
					//System.out.println(row[i]);
					if(row[i]==letter) {
						//System.out.println(row[i]);	
						//System.out.println(" found in the middle column "+i+"--row---"+indexRow);
						startSearchNodes(indexArrayWord, indexRow, i, letter);
					}
					continue;
				}
				
				if(row[i]==letter) {
					//System.out.println(row[i]);	
					//System.out.println(" found in the colun ---"+i+"--row---"+indexRow);
					startSearchNodes(indexArrayWord, indexRow, i, letter);
				}
				if(row[j]==letter) {
					//System.out.println(row[j]);
					//System.out.println(" found in the coumn ---"+j+"--row---"+indexRow);
					startSearchNodes(indexArrayWord, indexRow, j, letter);
				}
				// if the letter make match with one row, use the indexWordFind that is the array Index letter
				// and start the allgorit to seart with the word
				//onces you find the letter start one algorith that move in the word to find
				//move to the next chart in the word to find applay search nodes algorith, 
				// depend is you move to righ create a node right and if you move left 
				//create a left node and continue
				
				// once find the letter create a Nod, applay search close Node and depends on the direction
				// that you move create and specific node, Deque the node in the first Node. 
				// in the main Node apply the searchnext Node in the Dique for to that you can use a
				// default method in the interface
			}
		}
		
		void startSearchNodes(int indexArrayWord,int indexRow,int indCol,char letter) {
			// if you find the letter return a boolean  and write
			searKey=(String in)->{
				//in.charAt(0);
				if(mapList.containsKey(in)) return Optional.of(mapList.get(in));
				else { java.util.List<Nod> node= new java.util.ArrayList<Nod>(); mapList.put(in,node)  ;return Optional.of(node);}
			};
			Nod nod=new Nod(letter,indexArrayWord);
			nod.posRow=indexRow;
			nod.posCol=indCol;
			nod.posLetterFind=0;
			//System.out.println("the letter to find----"+letter+"----");
			nod.searchNode(this.grid, this.wordToFind);
			searchWeid(nod);
			//System.out.println("valor-----"+nod.toString()+"---------");
			//if after execute the searchNode and the list is empety you only need one node
			if(nod.takeOutWord(this.wordToFind[indexArrayWord]).toString().equals(this.wordToFind[indexArrayWord])) {
			Optional<java.util.List<Nod> >optList= searKey.apply(this.wordToFind[indexArrayWord]);
			optList.ifPresent((Lis)->Lis.add(nod));
			//if(optList.isPresent()) optList.get().add(nod);
			}
			
			
			// put on the map that letter to find and push one Node to the list of the letter
			
			//just here onece you have the nodes  applay interaln Nod method that allow the search in deep of the node
		}
		
		void searchWeid(Nod nod) {
			Deque<Nod> nodeProcess=new ArrayDeque<Main.Nod>();
			Nod node=null;
			while(!nod.deq.isEmpty()) {
			nod.searchWord(nod.deq, this.grid, this.wordToFind);
			node=nod.deq.poll();
			nodeProcess.add(node);
			}
			nod.deq=nodeProcess;
		}
		
		
		// wheen run the metho you are going to call the list of the node to the method 
		// making a recursive call of the each node.
		
		
		// matrix one by one is 0,0 the posicion
		
		//searchLetter
		// we are going to search the first letter in the matrix
		// to find the letter we go array to array is row  to row and applay the 
		// binary search java, once we found the number alias char, start the search Node algorith
		// the On for this search is NlOGn
		
		// The search node algotih use the first to x and y coordinates find in the last algorit
		// the first node is just as Nod as the first in the list
		// in game applay a method that  find Nod trying to find out the next Nod 
		
		
		
		
		
		
		
	}
	
	
	
	
	
	interface nextNode{
		
	void searchNode(char [][]mat,String [] find);
	
	default void searchWord(Queue<? extends nextNode> nde,char [][] mat,String []wordToFind) {
		if(nde.isEmpty()) return;
		nextNode top=nde.peek(); 
		Predicate<Nod> checkNod=s->s instanceof NodLeft;
		checkNod=checkNod.or(s->s instanceof NodRight).
		or(s->s instanceof NodDown ).or(s->s instanceof NodUp ).
		or(s->s instanceof NodDigUpLeft ).or(s->s instanceof NodDigUpRight ).
		or(s->s instanceof NodDigDownLeft ).or(s->s instanceof NodDigDownRight );
		if(top instanceof Nod && !checkNod.test((Nod)top)){
			nde.poll();
		}else {
		top.searchNode(mat, wordToFind);
		Nod nod=(Nod)top;
		//if(nod.deq.isEmpty()) {nde.poll(); searchWord(nde, mat,wordToFind); }// here he is delete the  node
		searchWord(nod.deq, mat, wordToFind);
		//nde.add(nod);
		}
	}
		
	}
		
	
	class Nod implements nextNode{
		int posRow;
		int posCol;
		int posLetterFind;
		final char a;
		final int indexWordToFind;
		final String out="%d %d"; 
		BiPredicate<Integer,Integer> check=(x,y)->y>=0 && y <= x;
		BiPredicate<Integer,Integer> next=Integer::equals;
		Deque<Nod> deq;
		
		public Nod(char in,int inde) {
			this.a=in;
			this.indexWordToFind=inde;
			deq=new ArrayDeque<Main.Nod>();
		}
		
		
		@Override
		public void searchNode(char[][] mat,String[] find) {
			boolean isup=check.test(mat.length-1,posRow-1);
			boolean isdown=check.test(mat.length-1,posRow+1);
			boolean isleft=check.test(mat[0].length-1,posCol-1);
			boolean isright=check.test(mat[0].length-1,posCol+1);
			boolean isdiagRightUp= isup && isright;
			boolean isdiagLeftDown = isleft && isdown;
			boolean isdiagLeftUp = isleft && isup;
			boolean isdiagRightDown = isright && isdown;
			Nod nod;
			boolean isva=check.test(find[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char nextChar=find[this.indexWordToFind].charAt(this.posLetterFind+1);
			
			if(isdiagRightDown && mat[posRow+1][posCol+1]==nextChar) {
				
				nod=new NodDigDownRight(nextChar,indexWordToFind);
				nod.posLetterFind=this.posLetterFind+1;
				nod.posRow=this.posRow+1;
				nod.posCol=this.posCol+1;
				deq.push(nod);
			}
			
			if(isdown && mat[posRow+1][posCol]==nextChar) {
				nod=new NodDown(nextChar,indexWordToFind);
				nod.posLetterFind=this.posLetterFind+1;
				nod.posRow=this.posRow+1;
				nod.posCol=this.posCol;
				deq.push(nod);
			}
			
			if(isright && mat[posRow][posCol+1]==nextChar) {
				nod=new NodRight(nextChar,indexWordToFind);
				nod.posLetterFind=this.posLetterFind+1;
				nod.posRow=this.posRow;
				nod.posCol=this.posCol+1;
				deq.push(nod);
			}
			
			if(isdiagRightUp && mat[posRow-1][posCol+1]==nextChar) {
				nod=new NodDigUpRight(nextChar,indexWordToFind);
				nod.posLetterFind=this.posLetterFind+1;
				nod.posRow=this.posRow-1;
				nod.posCol=this.posCol+1;
				deq.push(nod);
			}
			
			if(isdiagLeftDown && mat[posRow+1][posCol-1]==nextChar) {
				nod=new NodDigDownLeft(nextChar,indexWordToFind);
				nod.posLetterFind=this.posLetterFind+1;
				nod.posRow=this.posRow+1;
				nod.posCol=this.posCol-1;
				deq.push(nod);
				
			}
			
			if(isup && mat[posRow-1][posCol]==nextChar) {
				nod=new NodUp(nextChar,indexWordToFind);
				nod.posLetterFind=this.posLetterFind+1;
				nod.posRow=this.posRow-1;
				nod.posCol=this.posCol;
				deq.push(nod);
			}
			
			if(isleft && mat[posRow][posCol-1]==nextChar) {
				nod=new NodLeft(nextChar,indexWordToFind);
				nod.posLetterFind=this.posLetterFind+1;
				nod.posRow=this.posRow;
				nod.posCol=this.posCol-1;
				deq.push(nod);
			}
			
			if(isdiagLeftUp && mat[posRow-1][posCol-1]==nextChar) {
				nod=new NodDigUpLeft(nextChar,indexWordToFind);
				nod.posLetterFind=this.posLetterFind+1;
				nod.posRow=this.posRow-1;
				nod.posCol=this.posCol-1;
				deq.push(nod);
			}
			
			
		}
		
		
		public int getPosRow() {
			return posRow;
		}


		public void setPosRow(int posRow) {
			this.posRow = posRow;
		}


		public int getPosCol() {
			return posCol;
		}


		public void setPosCol(int posCol) {
			this.posCol = posCol;
		}


		private StringBuilder concatString(Nod no) {
			if(no.deq.isEmpty()) return  new StringBuilder().append(no.a);
			Nod nod=no.deq.poll();
			StringBuilder stri=new StringBuilder();
			stri.append(no.a);
			return stri.append(concatString(nod));
		}
		
		
		public StringBuilder takeOutWord(String compareString) {
			Nod nod;
			Supplier<StringBuilder> sup=StringBuilder::new;
			BiPredicate<String, String> val=String::equals;
			StringBuilder str=sup.get();
			str.append(this.a);
			if (this.deq.isEmpty()) return str;// no more node to search
			while((nod=this.deq.poll())!=null) {
				str.append(concatString(nod));
				//System.out.println("word take out ---"+str.toString()+"---");
				if (val.test(str.toString(),compareString)) break;
				str=sup.get();
				str.append(this.a);
			}
			if(str.toString().equals(sup.get().append(this.a).toString())) str=sup.get().append("NOT");
			return str;
		}
		@Override
		public String toString() {
			return String.format(this.out,this.posRow+1,posCol+1);
		}
		
	}
	
	class NodLeft extends Nod {
		
		public NodLeft(char a,int inde) {
			super(a,inde);
		}

		@Override
		public void searchNode(char[][] mat,String []wordToFind) {
			boolean isva=check.test(wordToFind[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char letterFind=wordToFind[indexWordToFind].charAt(posLetterFind+1);
			if(check.test(mat[0].length-1,this.posCol-1)) {
				if(next.test((int)mat[posRow][posCol-1],(int)letterFind)) {
					Nod nod=new NodLeft(letterFind,indexWordToFind);
					nod.posCol=this.posCol-1;
					nod.posRow=this.posRow;
					nod.posLetterFind=posLetterFind+1;
					this.deq.push(nod);
				}
			}
		}
		
	}
	
	class NodRight extends Nod {
		
		public NodRight (char a,int ind) {
			super(a,ind);
			
		}

		@Override
		public void searchNode(char[][] mat,String[] wordToFind) {
			boolean isva=check.test(wordToFind[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char letterFind=wordToFind[indexWordToFind].charAt(posLetterFind+1);
			if(check.test(mat[0].length-1,posCol+1)) {
				if(next.test((int)mat[posRow][posCol+1],(int)letterFind)) {
					Nod nod=new NodRight(letterFind,indexWordToFind);
					//asign the new posicion of the node
					nod.posCol=posCol+1;
					nod.posRow=posRow;
					nod.posLetterFind=posLetterFind+1;
					this.deq.push(nod);
				}
			}
		}
	}
	
	class NodUp extends Nod {
		
		public NodUp(char a,int ind) {
			super(a,ind);
		}

		@Override
		public void searchNode(char[][] mat, String [] wordToFind) {
			boolean isva=check.test(wordToFind[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char letterFind=wordToFind[indexWordToFind].charAt(posLetterFind+1);
			if(check.test(mat.length-1,posRow-1)) {
				if(next.test((int)mat[posRow-1][posCol],(int)letterFind)) {
					Nod nod=new NodUp(letterFind,indexWordToFind);
					nod.posCol=posCol;
					nod.posRow=posRow-1;
					nod.posLetterFind=posLetterFind+1;
					this.deq.push(nod);
					}
				}
		}
		
		
	}
	
	class NodDown extends Nod {
		
		 public NodDown(char a,int ind) {
			 super(a,ind);
		}

		@Override
		public void searchNode(char[][] mat,String[] wordToFind) {
			boolean isva=check.test(wordToFind[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char letterFind=wordToFind[indexWordToFind].charAt(posLetterFind+1);
			if(check.test(mat.length-1, posRow+1)) {
				if(next.test((int) mat[posRow+1][posCol],(int)letterFind )) {
					Nod nod=new NodDown(letterFind,indexWordToFind);
					nod.posCol=posCol;
					nod.posRow=posRow+1;
					nod.posLetterFind=posLetterFind+1;
					this.deq.push(nod);
					}
				}
		}
			
		
		
	}
	
	class NodDigUpRight extends Nod {
		public NodDigUpRight(char a,int ind) {
			super(a,ind);
		}

		@Override
		public void searchNode(char[][] mat,String [] wordToFind) {
			boolean isva=check.test(wordToFind[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char letterFind=wordToFind[indexWordToFind].charAt(posLetterFind+1);
			boolean isDigUpRight=check.test(mat.length-1, posRow-1) && check.test(mat[0].length-1, posCol+1);
			if(isDigUpRight) {
				if(next.test((int) mat[posRow-1][posCol+1],(int)letterFind )) {
				Nod nod=new NodDigUpRight(letterFind,indexWordToFind);
				nod.posCol=posCol+1;
				nod.posRow=posRow-1;
				nod.posLetterFind=posLetterFind+1;
				this.deq.push(nod);
				}
			}
		}
		
			
		
		
	}
	
	class NodDigDownRight extends Nod {
		
		public NodDigDownRight(char a,int ind) {
			super(a,ind);
		}

		@Override
		public void searchNode(char[][] mat,String[]wordToFind) {
			boolean isva=check.test(wordToFind[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char letterFind=wordToFind[indexWordToFind].charAt(posLetterFind+1);
			boolean isDigDownRight=check.test(mat.length-1, posRow+1) && check.test(mat[0].length-1, posCol+1);
			if(isDigDownRight) {
				if(next.test((int) mat[posRow+1][posCol+1],(int)letterFind )) {
					Nod nod=new NodDigDownRight(letterFind,indexWordToFind );
					nod.posCol=posCol+1;
					nod.posRow=posRow+1;
					nod.posLetterFind=posLetterFind+1;
					this.deq.push(nod);
				}
			}
			
		}
		
	}
	
	class NodDigDownLeft extends Nod {
		
		public NodDigDownLeft(char a,int ind) {
			super(a,ind);
		}

		@Override
		public void searchNode(char[][] mat,String []wordToFind) {
			boolean isva=check.test(wordToFind[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char letterFind=wordToFind[indexWordToFind].charAt(posLetterFind+1);
			boolean isDigDownLeft=check.test(mat.length-1, posRow+1) && check.test(mat[0].length-1, posCol-1); 
			if(isDigDownLeft) {
				if(next.test((int) mat[posRow+1][posCol-1],(int)letterFind )) {
					Nod nod=new NodDigDownLeft(letterFind,indexWordToFind);
					nod.posRow=posRow+1;
					nod.posCol=posCol-1;
					nod.posLetterFind=posLetterFind+1;
					this.deq.push(nod);
				}	
			}
		}
			
		
	}
	
	class NodDigUpLeft extends Nod {
		
		public NodDigUpLeft(char a,int ind) {
			super(a,ind);
		}

		@Override
		public void searchNode(char[][] mat,String []wordToFind) {
			boolean isva=check.test(wordToFind[indexWordToFind].length()-1, posLetterFind+1);
			if(!isva) return;
			char letterFind=wordToFind[indexWordToFind].charAt(posLetterFind+1);
			boolean isDigUpLeft=check.test(mat.length-1, posRow-1) && check.test(mat[0].length-1, posCol-1);
			if(isDigUpLeft){
			if(next.test((int) mat[posRow-1][posCol-1],(int)letterFind )) {
				Nod nod=new NodDigUpLeft(letterFind,indexWordToFind);
				nod.posCol=posCol-1;
				nod.posRow=posRow-1;
				nod.posLetterFind=posLetterFind+1;
				this.deq.add(nod);
				}
			}
		}
		
	}	
	
	
	

}
