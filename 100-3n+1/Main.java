import java.util.Arrays;
import java.util.OptionalInt;
import java.util.Scanner;
import java.util.stream.IntStream;


public class Main{

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		while(in.hasNextInt()) {
			/*if(line.isEmpty()) {
				System.exit(0);
				return ;
			}
			line=line.replaceAll("^\\s+", "");
			line=line.replaceAll("\\s+$", "");
			String buff[]=line.split("\\s");*/
			
			int start=in.nextInt(); //Integer.parseInt(buff[0]);
			int end=in.nextInt(); //Integer.parseInt(buff[1]);
			String startLine=start+" "+end;
			int []valuesCicleLength=new int[Math.abs((end-start))+1];
			int cont=0;
			int realend=Math.max(start,end);
			int realstart=Math.min(start,end);
		
			
			  
			for(;realstart<=realend;realstart++) {
				int out=ciclelengtg(realstart);
				valuesCicleLength[cont]=out;
				cont++;
			}
			 IntStream stream=Arrays.stream(valuesCicleLength);
			 OptionalInt optio=stream.max();
			 System.out.printf("%s %d%n",startLine,optio.getAsInt());
		}
	}
	
	
	
	static int ciclelengtg(int startAux) {
		int cont=1;
		if (startAux == 1) return startAux;
		do{
			++cont;
			startAux=startAux % 2 ==1?(3*startAux)+1:startAux/2;
		}while(startAux != 1);
		return cont;
	}

}
