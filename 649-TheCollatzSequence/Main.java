import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;

public class Main {
	
	static int count=1;
	static public void main(String...args) {
		BufferedReader buff=new BufferedReader(new InputStreamReader(System.in));
		boolean flagout=false;
		
		try {
		lab_to_out:
			while(!flagout) {
				BigDecimal numbers[]=getNumb(readString(buff));
				flagout=ifEnd(numbers);
				if(flagout) {break lab_to_out;}
				Sequenze seq=new Main().new Sequenze(count++,numbers[0],numbers[1]);
				System.out.println(seq);
		}
		}catch (Exception e) {
			//e.printStackTrace();
			System.exit(-1);
		}
	}
	
	static String readString(BufferedReader buff) throws IOException{
		String line=buff.readLine();
		if(line.isEmpty()) {
			StringBuilder str=new StringBuilder();
			int char_=0;
			while((char_=buff.read())!=-1) {
				str.append(char_);
			}
			line=str.toString();
		}
		line=line.trim();
		return line.replaceAll("\\s+", " ");
	}
	
	static BigDecimal[] getNumb(String line) {
		if (line.isEmpty()) {BigDecimal[] big= {new BigDecimal(-1),new BigDecimal(-1)};return big;}
		String spl[]=line.split(" ");
		BigDecimal big=new BigDecimal(spl[0]);
		BigDecimal big2=new BigDecimal(spl[1]);
		BigDecimal[] bigs={big,big2};
	    return bigs;
	}
	
	static boolean ifEnd(BigDecimal...arg) {
		long a=arg[0].longValue(),b=arg[1].longValue();
		if(a<0 & b<0) return true;
		else return false;
	}
	
	
	
	class Sequenze{
		
		final BigDecimal sequenceStart;
		final BigDecimal limit;
		final int round;
		int count=0;
		final String O_UT_for$="Case %d: A = %s, limit = %s, number of terms = %d";
		
		
		Sequenze(int round,BigDecimal...args) {
			this.round=round;
			this.sequenceStart=args[0];
			this.limit=args[1];
			calcSequence();
		}
		
		
		
		
		
		void calcSequence(){
			BigDecimal start=new BigDecimal(sequenceStart.toPlainString());
			//System.out.println(sequenceStart);
			calcSeq(start);
			//System.out.println(this.count);
		}
		
		void calcSeq(BigDecimal a) {
			BigDecimal cero=new BigDecimal(0);
			BigDecimal uno=new BigDecimal(1);
			BigDecimal dos=new BigDecimal(2);
			BigDecimal tres =new BigDecimal(3);
			if(a.equals(uno))  return;
			boolean resul= a.remainder(dos).equals(cero);
				if(resul) {
				a=a.divide(dos);
				this.count++;
				calcSeq(a);
				}
			else {
				 BigDecimal big1=a.multiply(tres);
				 BigDecimal big2=big1.add(uno);
				 a=big2;
				 BigDecimal max=this.limit.max(a);
				 if(!max.equals(limit)) return;// this dont work with fine-- here  i am checking the future value
				 this.count++;
				 calcSeq(a);
				}
		}
		
		
		
		@Override
		public String toString() {
			if(this.count==0)this.count++;
			if(this.count>1)this.count++;
			return String.format(O_UT_for$,round,sequenceStart.toPlainString(),limit.toPlainString(),this.count);
		}
		
	}
	
	
	

}
