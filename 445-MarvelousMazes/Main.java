import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {
	
	
	
	
	public static void main(String args[]) {
		BufferedReader read=new BufferedReader(new InputStreamReader(System.in));
		StringBuilder str=new StringBuilder();
		int charact=0;
		int acum=0;
		try
		{
			while((charact=read.read())!=-1) {
				if(Character.isDigit((char)charact)) {
					acum+=Character.getNumericValue((char)charact);
				}else if(Character.isLetter((char)charact) || ((char)charact)=='*' ) {
					Instruction ins=new Main().new Instruction((char)charact,acum);
					str.append(ins.toString());
					acum=0;
				}else if(((char)charact)=='!' ||((char)charact)=='\n' ){
					str.append('\n');
					System.out.print(str.toString());
					str=new StringBuilder();
				}
			}
		}catch (Exception e) {
			System.exit(-1);
		}
	}
	
	
	
	
	
	class Instruction {
		char charc;
		int space;
		
		Instruction(char cha,int space) {
			if(cha=='b')cha=' ';
			this.charc=cha;
			this.space=space;
		}
		
		@Override
		public String toString() {
			StringBuilder strB=new StringBuilder();
			for(int i=0;i<space;i++) {
				if(charc!='\n')strB.append(charc);
			}
			return  strB.toString();
		}
		
	}
	
	

}
