import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
	
	
	final static int MAX_COLGLOB=2;
	final static int MAX_COL=25;
	final static int MAX_INDEX=MAX_COL-1;
	final static String leftSpace="^\\s+";
	final static String rightSpace="\\s+$";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		boolean ban=false;
		BufferedReader buff=new BufferedReader(new InputStreamReader(System.in));
		while(!ban) {
			try {
			String line=buff.readLine();
			if(line.equals("0")) {ban=true;continue;};
			String arrayString [][];
			int rows=Integer.parseInt(line);
			arrayString=new String[rows][MAX_COLGLOB];
			boolean isSicke=false,globalSicke=false;
			for(int i=0;i<rows;i++) {
				String ini= buff.readLine();
				arrayString[i][0]=ini.replace(leftSpace,"");
				arrayString[i][0]=arrayString[i][0].replace(rightSpace, "");
				isSicke=!arrayString[i][0].contains(" ");
				if(!globalSicke) {
					globalSicke=isSicke?true:false;
				}
				StringBuilder str=new StringBuilder();
				if(isSicke) {
					str.append(i);
				}else {
					int countLeftX=countLeftX(arrayString[i][0],0);
					int posLeftX=countLeftX-1;
					str.append(posLeftX)
					.append(',').append(countLeftX).append(',')
					.append(i);
				}
				arrayString[i][1]=str.toString();
			}
			if(globalSicke) {
				int outCount=0;
				for(int i=0;i<rows;i++) {
					if(!arrayString[i][1].contains(",")) {
								//i++;
					}else {
						String split[]=arrayString[i][1].split(",");
						int []infRow=new int[] {
								Integer.parseInt(split[0]),
								Integer.parseInt(split[1])
						};
					outCount+=countLeftSpace(new StringBuilder(arrayString[i][0]),++infRow[0]);
					}
				}
				System.out.println(outCount);
			}else 
				
				{
					Map<Integer,int[]>higerPicture=new HashMap<Integer,int[]>();
					int higherPicture=0;
					for(int i=0;i<rows;i++) {
						int totalXRow=0;
						//System.out.println("procensando ---"+arrayString[i][0]+"---------");
						int countLeftX=countLeftX(arrayString[i][0],0);
						int countRightX=countRightX(arrayString[i][0],MAX_INDEX);
						int posLeftX=countLeftX-1;
						int posRighX=MAX_INDEX-countRightX;
						totalXRow=countLeftX +countRightX;
						int isLeft=Math.max(countLeftX,countRightX)==countLeftX?1:0;
						higherPicture=Math.max(higherPicture,totalXRow);
						
						StringBuilder str=new StringBuilder();
						str.append(posLeftX)
						.append(',').append(posRighX).append(',')
						.append(countLeftX).append(',').append(countRightX).append(',').append(i).append(',')
						.append(isLeft).append(',')
						.append(totalXRow);
						arrayString[i][1]=str.toString();
						higerPicture.put(totalXRow, new int[] {posLeftX,posRighX,countLeftX,countRightX,i,isLeft,higherPicture,totalXRow});
					}
					int[] sumdeltePicture=higerPicture.get(higherPicture);
					int spaceToDelete=MAX_COL-Math.abs(sumdeltePicture[2]+sumdeltePicture[3]);
					int leftSpace=0;
					for(int i=0;i<rows;i++) {
						leftSpace+=deleteCountSpace(arrayString[i][0], arrayString[i][1], spaceToDelete);
					}
					System.out.println(leftSpace);
				  }
			}catch (Exception e) {
				
			}
		}
		

	}
	
	
	static int countLeftX(String row,int index) {
		if(row.charAt(index)==' ') return 0;
		int out=1;
		out+=countLeftX(row, ++index);
		return out;
	}
	
	static int countLeftSpace(StringBuilder row,int index) {
		if(row.charAt(index)=='X') return 0;
		int out=1;
		out+=countLeftSpace(row, ++index);
		return out;
	}
	
	static int countRightX(String row,int index) {
		if(row.charAt(index)==' ') return 0;
		int out=1;
		out+=countRightX(row, --index);
		return out;
	}
	
	
	
	
	static int deleteCountSpace(String row, String info,int toDelete) {
		StringBuilder str=new StringBuilder(row);
		String split[]=info.split(",");
		int []infRow=new int[] {
				Integer.parseInt(split[0]),
				Integer.parseInt(split[1]),
				Integer.parseInt(split[2]),
				Integer.parseInt(split[3]),
				Integer.parseInt(split[4]),
				Integer.parseInt(split[5]),
				Integer.parseInt(split[6])
		};
		int posF=(toDelete+(infRow[0]+1));
		
		str=str.delete(++infRow[0],posF);
		return countLeftSpace(str,infRow[0]);
	}
	
	
	

}
