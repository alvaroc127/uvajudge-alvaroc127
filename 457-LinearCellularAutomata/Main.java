import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
	
	/**
	 * sum the poblation of each line by the way that they say(follow the rules) and the result of the sum if always <  9 
	 * then find in the DNA array the number  the index like the sum and use it with the poblation for the next day  
	 * 
	 */
	public static void main(String...args) {
		BufferedReader buff=new BufferedReader(new InputStreamReader(System.in));
		try {
			int times=readInt(buff);
			for(int i=0;i<times;i++) {
			 int dna[]=_dnaINarray_(readDNALine(buff));	
			 Main.Automata automa=new Main().new Automata(dna);
			 if(i+1!=times) System.out.println();
			}
		}catch (IOException e) {
			System.exit(-1);
		}
	}
	
	
	static Integer readInt(BufferedReader buff) throws IOException{
		String line=buff.readLine();
		line=line.trim();
		return Integer.valueOf(line);
	}
	
	static String readDNALine(BufferedReader buff) throws IOException{
		String line=buff.readLine();
		while(line.isEmpty()) {
			line=buff.readLine();
		}
		line=line.trim();
		line=line.replaceAll("\\s"," ");
		return line;
	}
	
	static int [] _dnaINarray_(String dna) {
		int[] dnaArray=new int[10];
		String arr[]=dna.split(" ");
		for(int i=0;i<arr.length;i++) {
			dnaArray[i]=Integer.parseInt(arr[i]);
		}
		return dnaArray;
	}
	
	
	
	private class Automata{
		
		final int [] dna;
		final static int WOR_50_DAY=50;
		final int populatio_Day_1_dis20$=1;
		final int position$_dish_position=19;
		Dish[][] current$$nextDayDish;
	
		
		
		Automata(int ad[]){
			this.dna=ad;
			current$$nextDayDish=new Dish[2][40];
			Arrays.fill(current$$nextDayDish[0], new Dish(0));
			current$$nextDayDish[0][position$_dish_position]=new Dish(populatio_Day_1_dis20$);
			for(int i=0;i<WOR_50_DAY;i++) {
				if(i>=20);
				calculatePopulationNextDay(current$$nextDayDish[0].length-1);
				printCurrentDay();
				Dish [] dishCopys=Arrays.copyOf(current$$nextDayDish[1], current$$nextDayDish[0].length);
				current$$nextDayDish[0]=dishCopys;
			}
			//current$$nextDayDish[0] covert all elements in string
			// for the next current day copy the array next in current and overwrite the next with new values
		}
		
		// The error start when the whole line have characters this mean before of the line was fill it up 
		// the algorith work but when the line is full somithing is wrong check de if to the end of the line
		// the sum is ok
		
		void calculatePopulationNextDay(int endIndeDish) {
			if(endIndeDish==0) { 
				int posici=addDish(current$$nextDayDish[0][endIndeDish+1],
						current$$nextDayDish[0][endIndeDish]);
				Dish dis=new Dish(dna[posici]);
				current$$nextDayDish[1][endIndeDish]=dis;
				return ;
			}
			if(endIndeDish==current$$nextDayDish[0].length-1) {
				int posici=addDish(current$$nextDayDish[0][endIndeDish-1],
						current$$nextDayDish[0][endIndeDish]);
				//System.out.print("values to sum (1)---"+current$$nextDayDish[0][endIndeDish-1].getPopulation());
				//System.out.println("values to sum (2)---"+current$$nextDayDish[0][endIndeDish].getPopulation());
				//System.out.println("posicion  -- "+posici +"DNA-------"+dna[posici]);
				
				
				Dish dis=new Dish(dna[posici]);
						current$$nextDayDish[1][endIndeDish]=dis;
				//System.out.println("calculed value "+dis.getPopulation()+"---"+dis.getCharPopula());
			}
			int partia$lpos=0;
			if(endIndeDish!=39)
			partia$lpos=addDish(current$$nextDayDish[0][endIndeDish+1],current$$nextDayDish[0][endIndeDish]);
			calculatePopulationNextDay(--endIndeDish);
			++endIndeDish;
			if(endIndeDish>0 && endIndeDish<39) {
				int out=partia$lpos+current$$nextDayDish[0][endIndeDish-1].getPopulation();
				//System.out.println("valor para el indice "+endIndeDish+" -- "+out);
			Dish dis=new Dish(dna[out]);
			current$$nextDayDish[1][endIndeDish]=dis;
			}
		}
		
		int addDish(Dish...di) {
			return di[0].getPopulation()+di[1].getPopulation();	
		}
		
		public void printCurrentDay() {
			StringBuilder str=new StringBuilder();
			for(Dish  dish: current$$nextDayDish[0]) {
				str.append(dish.getCharPopula());
			}
			System.out.println(str.toString());
		}
	}
	
	
	class Dish {
		
		private int population;
		private char charPopula;
		
		public Dish(int pupulation) {
			this.population=pupulation;
			switch (population) {
			case 0:
				charPopula=' ';
				break ;
			case 1:
				charPopula='.';
			break;
			
			case 2:
				charPopula='x';
			break;
			
			case 3:
				charPopula='W';
			break;

		  default:
			charPopula='J';
			break;
		  }
		}
		
		
		public int getPopulation() {
			return population;
		}
		
		public void setPopulation(int populat) {
			this.population+=populat;
			
		}

		public char getCharPopula() {
			return charPopula;
		}

		
	}
	
	
	

}
