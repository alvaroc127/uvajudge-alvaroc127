import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	
	
	
	public static final void main(String...args) {
		BufferedReader buff=new BufferedReader(new InputStreamReader(System.in));
		try {
			boolean cont=true;
		whileb: while(cont) {
			String lin=readLine(buff);
			if(lin.isEmpty()) { cont=false; break whileb;}
			ProWords pro=new Main().new ProWords(lin);
			System.out.println(pro);
			System.out.println();
			
		 }
		}catch (Exception e) {
			System.exit(-1);
		}
		
	}
	
	
	static String readLine(BufferedReader buff) throws IOException {
		String line=buff.readLine();
		if (null==line) return new String();
		if(!line.isEmpty()) return line.trim();
		int char1;
		StringBuilder strou=new StringBuilder();
		while((char1=buff.read())!=-1) 
		{
			strou.append(char1);
		}
		
		return strou.toString().replace('0','O');
	}
	
	
	
		
	
	class ProWords implements Palidrome,Mirror{
		
		final static String NOT_PALIN="%s -- is not a palindrome.";
		final static String PALINDRO="%s -- is a regular palindrome.";
		final static String MIRRORWORD="%s -- is a mirrored string.";
		final static String MIRRORPALINDRO="%s -- is a mirrored palindrome.";
		
		
		final String baseWord;
		final boolean isPalindrome;
		boolean isMirror;
		final boolean mirrorPalindL;
		
		
		public ProWords(String base) {
			baseWord=base;
			isPalindrome=isPalindrom(base);
			try {
				isMirror=mirror(baseWord);
			}catch (IllegalArgumentException e) {
				//e.printStackTrace();
				isMirror=false;
			}
			mirrorPalindL= isPalindrome && isMirror;
		}
		
		public char reverse(char a) throws IllegalArgumentException {
			StringBuilder str=new StringBuilder();
			if(Character.isDigit(a)){
				str.append('a').append('_').append(a);
			}else {
			str.append(a);
			}
			//System.out.println("---"+str.toString()+"--------");
			Reverse revers=Reverse.valueOf(str.toString());
			return revers.getRevert();
		}
		
		public StringBuilder mirrorString(String baseWord,int index) throws IllegalArgumentException {
			//System.out.println("base WOrd chart iNDEX---"+baseWord.charAt(index)+"-XXX-------");
			if(index==0) { StringBuilder str=new StringBuilder();return str.append(reverse(baseWord.charAt(index)));}
			StringBuilder str=new StringBuilder();
			str.append(reverse(baseWord.charAt(index)));
			return str.append(mirrorString(baseWord,--index));// here you have to check if the charIn the current index have revert
		
		}
		
		@Override
		public String toString() {
			boolean boleanresul= isPalindrome && isMirror;
			String out="";
			if(boleanresul) return String.format(MIRRORPALINDRO, baseWord);
			else {
				if(!isPalindrome) if(isMirror) out=String.format(MIRRORWORD, baseWord); else out=String.format(NOT_PALIN, baseWord) ;
				else out=String.format(PALINDRO,baseWord);
				return out;
			}
		}
		
	}
	
	
	interface Palidrome{
		
		default boolean isPalindrom(String baseWord) {
			StringBuilder str=new StringBuilder(baseWord);
			return str.reverse().toString().equals(baseWord);
		}
		
		
	}
	
	interface Mirror{
		
		StringBuilder mirrorString(String baseWor,int index ) throws IllegalArgumentException;
		
		default boolean mirror(String baseWord) throws IllegalArgumentException{
			StringBuilder outMirror=mirrorString(baseWord,baseWord.length()-1 );
			//System.out.println("cadena reversada>>>>"+outMirror+"<<valorrr");
			
			return outMirror.toString().equals(baseWord);
		}
		
		
		enum Reverse{
			//here put the equivalent chars
			A('A'),E('3'),H('H'),I('I'),
			J('L'),L('J'),M('M'),O('O'),
			S('2'),T('T'),U('U'),V('V'),
			W('W'),X('X'),Y('Y'),Z('5'),
		   a_1('1'),a_2('S'),a_3('E'),a_5('Z'),
		   a_8('8');
			
			
			final char revert;
			
			
			
			Reverse(char  revert)
			{
				this.revert=revert ;
			}			
			
			char getRevert(){
				return this.revert;
			}
		}
		
	}
	
	

}
