import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        BufferedReader buff = new BufferedReader(new InputStreamReader(System.in));
        try {
            var numberTime = Integer.valueOf(buff.readLine());
            int count = 0;
            while (count < numberTime) {
                readLine(buff, count + 1 == numberTime);
                count++;
            }
        } catch (IOException ex) {
            System.exit(-1);
        }
    }

    private static void readLine(BufferedReader bufferRead, boolean lastLine) throws IOException {
        String line1 = bufferRead.readLine();
        String line2 = "";
        if (lastLine) line2 = readEndLine(bufferRead);
        else line2 = bufferRead.readLine();
        OrderShutter order=new Main().new OrderShutter(line1,line2);
        System.out.println(order);
    }

    private static String readEndLine(BufferedReader buff) throws IOException {
        StringBuilder str = new StringBuilder();
        var val = 0;
        var coutnbreak = 0;
        tag_brak:
        while ((val = buff.read()) != -1) {
            str.append((char)val);
            if (val == '.') coutnbreak++;
            else coutnbreak=0;
            if (coutnbreak == 3) break tag_brak;
        }
        return str.toString();
    }

    interface ExtractSs {
       OrderShutter extract(PairShttelreim pair);
    }

    interface Cleanline {
        String cleanLine(StringBuilder line);
    }

    abstract class PairShttelreim {
        StringBuilder line1;
        StringBuilder line2;

        PairShttelreim(){
           super();
        }
        PairShttelreim(String s1,String s2){
            super();
            this.line1=new StringBuilder(s1);
            this.line2=new StringBuilder(s2);
        }

        public String cleanLine() {
            StringBuilder copyLine = new StringBuilder(line1.toString());
            //StringBuilder copyLine2 = new StringBuilder(line2.toString());
            Stream<StringBuilder> streamBuil = Stream.of(copyLine);
            Cleanline cline1 = (x) -> {
                return x.toString().replaceAll("<","");
            };
            Function<StringBuilder, String> funcMap = (input) -> {
                return cline1.cleanLine(input).replaceAll(">","");
            };
            return streamBuil.map(funcMap).reduce("",String::concat);
        }

    }
    class OrderShutter extends PairShttelreim {
        private String s1;
        private String s2;
        private String s3;
        private String s4;
        private String s5;

        OrderShutter(){
            super();
        }

        OrderShutter(String s1,String s2){
            super(s1,s2);
        }

        void extractSrs(){
            int firstindex=super.line1.indexOf("<");
            int closeFirstI$nd=super.line1.indexOf(">");
            int secondIndex=super.line1.indexOf("<",firstindex+1);
            int closeSecond$_=super.line1.indexOf(">",closeFirstI$nd+1);
            if(0-firstindex==0)s1="";else s1=super.line1.substring(0,firstindex);
            if(closeFirstI$nd-firstindex==0) s2="";else  s2=super.line1.substring(firstindex+1,closeFirstI$nd);
            if(secondIndex-closeFirstI$nd==0)s3="";else s3=super.line1.substring(closeFirstI$nd+1,secondIndex);
            if(closeSecond$_-secondIndex==0) s4="";else s4=super.line1.substring(secondIndex+1,closeSecond$_);
            if(closeSecond$_-super.line1.length()-1==0)s5="";s5=super.line1.substring(closeSecond$_+1);
            //System.out.println(s1+"------"+s2+"---------"+s3+"-----------"+s4+"----------"+s5);
        }

       String buildShutterlreim(){
            StringBuilder strCocat=new StringBuilder(s4);
            strCocat.append(s3).append(s2).append(s5);
            Supplier<StringBuilder> sup=StringBuilder::new;
            StringBuilder copyCat=sup.get();
            copyCat.append(super.line2);
            var beginEnd=copyCat.lastIndexOf("...");
            copyCat.replace(beginEnd,super.line2.length(),strCocat.toString());
            return copyCat.toString();
        }


        @Override
        public String toString() {
            StringBuilder str=new StringBuilder(super.cleanLine());
            extractSrs();
            str.append('\n');
            str.append(buildShutterlreim());
            return str.toString();
        }
    }
}