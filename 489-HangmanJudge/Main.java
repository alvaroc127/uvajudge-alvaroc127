import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	
	
	
	public static void main(String...args) {
		BufferedReader buff=new BufferedReader(new InputStreamReader(System.in));
		boolean continu=true;
		
		try {
			lab_whil: while(continu) 
			{
				int round=readInt(buff);
				if (round==-1) {continu=false; break lab_whil;}
				String answer=readString(buff);
				String attemp=readString(buff);
				HangMan game=new Main().new HangMan(round,answer,attemp);
				System.out.print(game);
			}
		}catch (Exception e) {
			System.exit(-1);
		}
	}
	
	
	static int readInt(BufferedReader buff) throws IOException {
		String value=buff.readLine();
		if(value.isEmpty()) {
			return buff.read();
		}else return Integer.parseInt(value);
	}
	
	
	static String readString(BufferedReader buff) throws IOException {
		return buff.readLine();
	}
	
	
	
	class HangMan {
		
		final static String CHICKENS_OUT="You chickened out.";
		final static String LOSES="You lose.";
		final static String WIN="You win.";
		final static String ROUND="Round %d";
		private int tries=7;
		String  guessToword;
		String 	setofWords;
		int round;
		java.util.Map<String,Try> game;
		
		public HangMan(int round,String...word) {
			this.guessToword=word[0];
			this.setofWords=word[1];
			this.round=round;
			game=new java.util.HashMap<String,Try>();
		}
		
		
		void playGame() {
			for(int i=0;i<setofWords.length()&&tries!=0;i++){
				if(!isSolvedBefore(setofWords.charAt(i))) {
					Try tri=this.new Try(setofWords.charAt(i));
					game.put(tri.toString(), tri);
					if(!tri.isCorrect)tries--;
				}
			}
		}
		
		boolean isSolvedBefore(char findOut) {
			StringBuilder buil=new StringBuilder();
			buil.append(findOut);
			boolean out=this.game.containsKey(buil.toString());
			return out;
		}
		
		
		boolean iTisGain() {
			//int totalCount=0;
			//System.out.println(game.size());
			// if the size of the array is 1 return the validation with the number of character of the try
			int total=game.values().stream().filter((tr)->{
				return tr.isCorrect;
			}).map((tra)->{
				return tra.numCharFound;
			}).reduce( 0,(a,b)->a+b);
			//System.out.println("read value "+total);
			/*for(Map.Entry<String, Try>entrySet:game.entrySet()) {
				totalCount+=entrySet.getValue().isCorrect?entrySet.getValue().numCharFound:0;
			}*/
			//System.out.println("read value"+totalCount);
			return total==guessToword.length();
		}
		
		//save the state of the game, we need to save the tries each state save if it was sucees or wrong, it saves the char guess and the position, when it will be wrong "tries" var is reduce in HangMang,
		//HangMan may to have an array or a LinkedObject or a LinkedList
		// we can put a hasmap and a linked of objects
		
		
		
		@Override
		public String toString() {
			// WHether the return of this method is true them guess the quest, but it is false and the tries is major to zero 
			// him was a chiken out but it arent' tries is a bad game
			playGame();
			StringBuilder str=new StringBuilder();
			str.append(String.format(ROUND,round));
			str.append('\n');
			// append the correct string with the jump line
			if(iTisGain()) str.append(WIN);
			else if(tries<=0) str.append(LOSES);
			else str.append(CHICKENS_OUT);
			str.append('\n');
			return str.toString();
		}
		
		class Try{
			
			char tryied_char;
			boolean isCorrect;
			int numCharFound;
			
			public Try(char charTried) {
				tryied_char=charTried;
				findChar();
			}
			
			
			void findChar(){
				int out=guessToword.indexOf(tryied_char);
				if(out!=-1) {
					out++;
					isCorrect=true;
					numCharFound++;
					//System.out.println("valur of "+numCharFound+"-***"+out);
					while((out=guessToword.indexOf(tryied_char,out))!=-1) {
						numCharFound++;
						out++;
					}
				}
				//System.out.println("valur of "+numCharFound+"-***"+out);
			}
			
			
			@Override
			public String toString() {
				StringBuilder str=new StringBuilder();
				str.append(tryied_char);
				return str.toString();
			   }
			
			}
			
			
		}
	
	
	
	
	
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


