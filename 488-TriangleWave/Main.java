import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	
// check when is online one you have to print one to the frecuenci	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BufferedReader buff=new BufferedReader(new InputStreamReader(System.in));
		try {
			
				Integer numcase=readInt(buff);
				int cont=0;
				while(cont<numcase) {
				Integer amplitude=readInt(buff);
				Integer frecuency=readInt(buff);
				Main main=new Main();
				Wave wav=main.new Wave(amplitude,frecuency);
				if(cont==numcase-1) {
					System.out.println(wav);
				}else {
					System.out.println(wav);
					System.out.println();
				}
				cont++;
			}
		}catch (Exception e) {
			System.exit(-1);
		}
		

	}
	
	static Integer readInt(BufferedReader buff) throws IOException{
		try {
			String line=null;
			Integer nextIn=null;
		while((line=buff.readLine())!=null) {
			if(line.isEmpty()) {
				continue;
			}
			return nextIn=Integer.parseInt(line);
		}
		return nextIn;
		}catch (Exception e) {
			throw e; 
		}
	}
	
	
	class Wave {
		int amplitude;
		int frecuency;
		
		public Wave(int amplitude,int frecuency) {
			if(amplitude>9)amplitude=9;
			this.amplitude=amplitude;
			this.frecuency=frecuency;
		}
		
		
		
		StringBuilder buildLinesWave() {
			StringBuilder waveLines=new StringBuilder();
			StringBuilder waveLines2=new StringBuilder();
			int stati=1;
			/*for(int i=0;i<amplitude;i++) {
				waveLines.append(buildLineWave(stati,i+1));
				if(i+1!=amplitude) {
					waveLines.append('\n');
				}
			}*/
			if (amplitude ==1) {waveLines.append("1");waveLines.append("\n");  return waveLines;}
			
			
			for(int i=0,j=amplitude;i<amplitude&&j>1;i++,j--) {
				waveLines.append(buildLineWave(stati,i+1));
				waveLines2.append(buildLineWave(stati,j-1));
				//System.out.print(waveLines+"----"+waveLines2);
					waveLines.append('\n');
					waveLines2.append('\n');
					if(i+1==amplitude-1) {
						waveLines.append(buildLineWave(stati,amplitude));
						waveLines.append('\n');
					}
			}
			waveLines.append(waveLines2);
			return waveLines;
		}
		
		StringBuilder buildLineWave(int base,int times) {
			StringBuilder str=new StringBuilder();
			if(times == base) {str.append(times); return str; } 
			str.append(times);
			str.append(buildLineWave(++base, times));
			return str;
		} 
		
		
		
		@Override
		public String toString() {
			StringBuilder wave=buildLinesWave();
			StringBuilder completeWave=new StringBuilder();
			for(int i=0;i<frecuency;i++) {
				completeWave.append(wave);
				if(i!=frecuency-1)completeWave.append('\n');
				else if (i==frecuency-1) completeWave.deleteCharAt(completeWave.length()-1);
			}
			return completeWave.toString();
		}
		
		
	}

	
	
	
}
